ipython==8.12.3
matplotlib==3.9.2
numpy==2.1.1
pandas==2.2.2
scikit_learn==1.5.1
seaborn==0.13.2
torch==2.4.0.post301
torch_summary==1.4.5
