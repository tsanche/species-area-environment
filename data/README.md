## Target variables
- `log_sr`: log of species richness 

## Feature values
- `log_area`
- `bio1`, ..., `bio15`: mean environmental predictors
- `std_*`: standard value of the environmental predictor, proxy for heterogeneity
- `habitat_id`: habitat type.

## Misc
- `partition`: the spatial partition ID. Can be used for spatial block cross validation.